package vinayak.apps.flupertest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import vinayak.apps.flupertest.database.DatabaseHelper;

public class CreateProductActivity extends AppCompatActivity {


    EditText prdName,prdDescription,sellPriceText,regularPriceText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);


        regularPriceText = findViewById(R.id.regularPrice);
        sellPriceText = findViewById(R.id.sellingPrice);
        prdDescription = findViewById(R.id.productDesc);
        prdName = findViewById(R.id.productName);


    }

    public void createProduct(View v)
    {
                try {
                    if(prdName.getText().toString().equalsIgnoreCase("") ||
                            prdDescription.getText().toString().equalsIgnoreCase("")||regularPriceText.getText().toString().equalsIgnoreCase("")|| sellPriceText.getText().toString().equalsIgnoreCase(""))
                    {
                        Toast.makeText(this,"Please fill all data",Toast.LENGTH_LONG).show();
                    }
                  else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("product_name", prdName.getText().toString());
                        jsonObject.put("sail_price", sellPriceText.getText().toString());
                        jsonObject.put("regular_price", regularPriceText.getText().toString());
                        jsonObject.put("description", prdDescription.getText().toString());
                        JSONArray jsonArray = new JSONArray();
                        jsonArray.put("#eacde1");
                        jsonArray.put("#ea00e1");
                        jsonArray.put("#eacd00");
                        jsonObject.put("colors", jsonArray);
                        jsonObject.put("image", "https://i.pinimg.com/originals/f4/a0/d6/f4a0d6cfd51432aab8387f000ee49fa4.jpg");

                        DatabaseHelper dbHelper = new DatabaseHelper(this);
                        SQLiteDatabase db = dbHelper.getWritableDatabase();
                        ContentValues cv = new ContentValues();
                        cv.put("prdData", jsonObject.toString());
                        float row = db.insert("ProductTable", null, cv);
                        dbHelper.close();
                        Toast.makeText(this,""+row,Toast.LENGTH_LONG).show();
                    }
                }
                catch(Exception e)
                {

                }

    }
}
