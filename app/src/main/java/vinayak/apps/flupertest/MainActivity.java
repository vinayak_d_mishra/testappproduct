package vinayak.apps.flupertest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    AppCompatButton createProductButton,showProductButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createProductButton = findViewById(R.id.create_product);
        showProductButton = findViewById(R.id.show_product);

        createProductButton.setOnClickListener(this);
        showProductButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.show_product)
        {

            startActivity(new Intent(this,ShowProductActivity.class));

        }
        if(view.getId() == R.id.create_product)
        {
            startActivity(new Intent(this,CreateProductActivity.class));
        }
    }
}
