package vinayak.apps.flupertest;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;

import vinayak.apps.flupertest.adapter.ProductAdapter;
import vinayak.apps.flupertest.database.DatabaseHelper;
import vinayak.apps.flupertest.model.Product;

public class ShowProductActivity extends AppCompatActivity {

    ArrayList<Product> products;
ListView  listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product);

        products = new ArrayList<>();
        listView = findViewById(R.id.list_view);

        loadProduct();

    }

    private void loadProduct()
    {
        products.clear();
        DatabaseHelper dbhelper = new DatabaseHelper(this);
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        Cursor cursor = db.query("ProductTable", null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            String data = cursor.getString(1);
            try {
                JSONObject jsonObject = new JSONObject(data);
                Product product = new Product();
                product.setProductId(cursor.getString(0));
                product.setProductName(jsonObject.getString("product_name"));
                product.setProductDescription(jsonObject.getString("description"));
                product.setProductImage(jsonObject.getString("image"));
                product.setSalePrice(jsonObject.getString("sail_price"));
                product.setRegularPrice(jsonObject.getString("regular_price"));
                products.add(product);
            } catch (Exception e) {

            }
        }

        listView.setAdapter(new ProductAdapter(this,products,delete,editClick,viewImage));

    }

    View.OnClickListener editClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    View.OnClickListener delete = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            int pos = (int)view.getTag();

            DatabaseHelper dbHelper = new DatabaseHelper(ShowProductActivity.this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete("ProductTable","_id = ?", new String[]{products.get(pos).getProductId()});
            db.close();
            dbHelper.close();
            loadProduct();

        }
    };

    View.OnClickListener viewImage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int pos = (int) view.getTag();


            AlertDialog.Builder ImageDialog = new AlertDialog.Builder(ShowProductActivity.this);
            ImageDialog.setTitle("");
            LayoutInflater inflater = getLayoutInflater();
            View dialoglayout = inflater.inflate(R.layout.image_view_full, null);

            ImageView imageView = dialoglayout.findViewById(R.id.prdImage);
            ImageDialog.setView(dialoglayout);
            Glide.with(ShowProductActivity.this).load(products.get(pos).getProductImage()).into(imageView);

            ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface arg0, int arg1)
                {
                }
            });

            ImageDialog.show();



        }
    };
}
