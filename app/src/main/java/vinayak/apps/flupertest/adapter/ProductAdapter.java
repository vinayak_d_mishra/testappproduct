package vinayak.apps.flupertest.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import vinayak.apps.flupertest.R;
import vinayak.apps.flupertest.model.Product;

public class ProductAdapter extends BaseAdapter
{

    Context mContext;
    ArrayList<Product> prdList;
    LayoutInflater mInflater;
    View.OnClickListener editClick,deleteClick,viewImage;

    public ProductAdapter(Context mContext, ArrayList<Product> productList, View.OnClickListener deleteClicked,
                          View.OnClickListener editClicked, View.OnClickListener viewimage)
    {
        this.mContext = mContext;
        prdList = productList;
        deleteClick = deleteClicked;
        editClick = editClicked;
        viewImage = viewimage;
        mInflater= LayoutInflater.from(this.mContext);

    }
    @Override
    public int getCount() {
        return prdList.size();
    }

    @Override
    public Product getItem(int i) {
        return prdList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        final ViewHolder holder;


        if(convertView==null){
            convertView=mInflater.inflate(R.layout.list_item_row,null);// R.layout.listitem_layout is your custom layout file

            holder=new ViewHolder();

            holder.prdDescLabel=convertView.findViewById(R.id.prdDescLabel);
            holder.prdNameLabel=convertView.findViewById(R.id.prdNameLabel);
            holder.prdImage=convertView.findViewById(R.id.prdImage);
            holder.edit = convertView.findViewById(R.id.edit);
            holder.delete = convertView.findViewById(R.id.delete);
            holder.delete.setTag(i);
            holder.edit.setTag(i);
            holder.prdImage.setTag(i);
            holder.prdImage.setOnClickListener(viewImage);
            holder.delete.setOnClickListener(deleteClick);
            holder.edit.setOnClickListener(editClick);
            convertView.setTag(holder);
        }
        else{
            holder=(ViewHolder)convertView.getTag();
        }

        Product product=getItem(i);

        holder.prdNameLabel.setText(product.getProductName());
        holder.prdDescLabel.setText(product.getProductDescription());
        Glide.with(mContext).load(product.getProductImage()).into(holder.prdImage);

        return convertView;
    }
    class ViewHolder{
        TextView prdNameLabel,prdDescLabel;
        ImageView prdImage;
        Button delete,edit;
    }

}
