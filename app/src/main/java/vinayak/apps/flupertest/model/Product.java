package vinayak.apps.flupertest.model;

import org.json.JSONArray;

public class Product
{
    String productId,productName,productDescription,regularPrice,salePrice,productImage;
    JSONArray colors;

    public void setColors(JSONArray colors) {
        this.colors = colors;
    }

    public JSONArray getColors() {
        return colors;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductImage() {
        return productImage;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public String getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getRegularPrice() {
        return regularPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setRegularPrice(String regularPrice) {
        this.regularPrice = regularPrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }
}
